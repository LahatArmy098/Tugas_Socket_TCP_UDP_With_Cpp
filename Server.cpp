// Server.cpp : Defines the entry point for the console application.
//
//Server
#include "stdafx.h"
#pragma comment(lib, "ws2_32.lib")
#define _WINSOCK_DEPRECATED_NO_WARNINGS 
#include <WinSock2.h>
#include <iostream>

SOCKET Connections[100];
int ConnectionCounter = 0;

int  ClientHandlerThread(int index) {
	char buffer[256];
	while (true) {
		recv(Connections[index], buffer, sizeof(buffer), NULL);
		for (int i = 0; i < ConnectionCounter; i++) {
			if (i == index)
				continue;
			send(Connections[i], buffer, sizeof(buffer), NULL);
		}
	}
}

int main()
{
	//winsock startup	
	WSAData wsaData;
	WORD DllVersion = MAKEWORD(2, 1);
	if (WSAStartup(DllVersion, &wsaData) != 0) // jika WSAStartup returns selain 0, maka terjadi error
	{
		MessageBoxA(NULL, "Winsock Startup Failed", "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}

	sockaddr_in addr; //alamat yang akan di bind oleh socket nantinya
	int addrlen = sizeof(addr); //panjang alamat (digunakan untuk menerima panggilan)
	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); //Local Broadcast
	addr.sin_port = htons(1111); //port 
	addr.sin_family = AF_INET; // IPV4 Socket

	SOCKET sListen = socket(AF_INET, SOCK_STREAM, NULL); //
	bind(sListen, (SOCKADDR*)&addr, sizeof(addr)); // bind alamat ke socket
	listen(sListen, SOMAXCONN); // membuat sListen socket dalam keadaan listening untuk terhubung

	SOCKET newConnection; //socket untuk menahan koneksi client
	for (int i = 0; i < 100; i++) {
		newConnection = accept(sListen, (SOCKADDR*)&addr, &addrlen); //menerima koneksi baru
		if (newConnection == 0) //jika penerimaan koneksi dari client gagal
		{
			std::cout << "Failed to accept the client's Connection." << std::endl;
		}
		else // jika client terhubung 
		{
			std::cout << "Client Connected" << std::endl;
			char MOTD[256] = "Selamat Datang";
			send(newConnection, MOTD, sizeof(MOTD), NULL);
			Connections[i] = newConnection;
			ConnectionCounter += 1;
			CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandlerThread, (LPVOID)(i), NULL, NULL); //
		}
	}
	system("pause");
    return 0;
}

