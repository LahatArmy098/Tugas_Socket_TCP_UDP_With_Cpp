// Client asli.cpp : Defines the entry point for the console application.
//
//Client
#include "stdafx.h"
#pragma comment(lib, "ws2_32.lib")
#define _WINSOCK_DEPRECATED_NO_WARNINGS 
#include <WinSock2.h>
#include <iostream>

SOCKET Connection;

void ClientThread()
{
	char buffer[256];
	while (true)
	{
		recv(Connection, buffer, sizeof(buffer), NULL);
		std::cout << buffer << std::endl;
	}
}


int main()
{
	//winsock startup
	WSAData wsaData;
	WORD DllVersion = MAKEWORD(2, 1);
	if (WSAStartup(DllVersion, &wsaData) != 0) // jika WSAStartup returns selain 0, maka terjadi error
	{
		MessageBoxA(NULL, "Winsock Startup Failed", "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}


	SOCKADDR_IN addr; //alamat yang akan di bind oleh socket nantinya
	int addrlen = sizeof(addr); //panjang alamat (digunakan untuk menerima panggilan)
	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); //Local Broadcast
	addr.sin_port = htons(1111); //port 
	addr.sin_family = AF_INET; // IPV4 Socket

	Connection = socket(AF_INET, SOCK_STREAM, NULL);
	if (connect(Connection, (SOCKADDR*)&addr, sizeof(addr)) != 0) //jika tidak dapat terhubung
	{
		MessageBoxA(NULL, "Failed to Connect", "Error", MB_OK | MB_ICONERROR);
		return 0; //koneksi gagal
	}
	std::cout << "Connected!" << std::endl;
	CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientThread, NULL, NULL, NULL); 

	char buffer[256];
	while (true)
	{
		std::cin.getline(buffer, sizeof(buffer));
		send(Connection, buffer, sizeof(buffer), NULL);
		Sleep(10);
	}

	system("pause");
	return 0;
}